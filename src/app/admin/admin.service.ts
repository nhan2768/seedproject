import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AdminPageModels } from './admin.model';

@Injectable()
export class AdminService {
    public getClient = 'rest/apibridge/clients';
    list: any;
    constructor(private http: HttpClient) { }

    getClients() {
        return this.http.get(this.getClient).map((res: Response) => {
            const data = JSON.parse(res.toString());
            this.list = new AdminPageModels(data);
            return this.list.getData();
        }).catch((res: Response) => {
            const data = res;
            return Observable.throw(data);
        });
    }
}