export class AdminPageModel {
    public type: string;
    public accessToken: string;
    public applicationId: string;
    public company: string;
    public domain: string;
    public email: string;
    public expirationDate: string;
    public firstName: string;
    public id: string;
    public lastName: string;
    public sourceChannel: string;
    public latitude: string;
    public longitude: string;
    public status: string;
    public updateVersion: string;
    constructor(item: any) {
        this.type = item.type || '';
        this.accessToken = item.accessToken || '';
        this.applicationId = item.applicationId || '';
        this.company = item.company || '';
        this.domain = item.domain || '';
        this.email = item.email || '';
        this.expirationDate = item.expirationDate || '';
        this.firstName = item.firstName || '';
        this.id = item.id || '';
        this.lastName = item.lastName || '';
        this.sourceChannel = item.sourceChannel || '';
        this.status = item.status || '';
        this.updateVersion = item.updateVersion || '';
        return this;
    }
    getData() {
        return {
            'type': this.type,
            'accessToken': this.accessToken,
            'applicationId': this.applicationId,
            'company': this.company,
            'domain': this.domain,
            'email': this.email,
            'expirationDate': this.expirationDate,
            'firstName': this.firstName,
            'id': this.id,
            'lastName': this.lastName,
            'sourceChannel': this.sourceChannel,
            'status': this.status,
            'updateVersion': this.updateVersion
        };
    }
  
}
export class AdminPageModels {
    listItem: any[];
    constructor(listItem: AdminPageModel[] = []) {
        this.listItem = listItem.map((item: AdminPageModel) => {
            return new AdminPageModel(item);
        });
    }
    getData() {
        return this.listItem.map((item: AdminPageModel) => item.getData());
    }
}
