import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { ShareModule } from '../shared/shared.module';
import { AdminComponent } from './admin.component';
import { AdminService } from './admin.service';

@NgModule({
    imports: [
        FormsModule,
        ShareModule,
        RouterModule
    ],
    declarations: [
        AdminComponent
    ],
    entryComponents: [],
    providers: [
        AuthService,
        AdminService
    ]
})
export class AdminModule { }
