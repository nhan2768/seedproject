import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  listItem: any;
  adminSetting = {
    columns: {
      description: {
        title: 'description',
        filter: false
      },
      address: {
        title: 'address',
        filter: false
      },
      city: {
        title: 'city',
        filter: false
      },
      phone_number: {
        title: 'phone number',
        filter: false
      },
      email: {
        title: 'email',
        filter: false
      },
      price_limit: {
        title: 'price limit',
        filter: false
      },
      time_open: {
        title: 'time open',
        filter: false
      },
      time_close: {
        title: 'time close',
        filter: false
      },
      action: {
        title: 'Action',
        filter: false
      }
    },
    actions: false,
    pager: {
      display: false,
      perPage: 30
    },
    noDataMessage: 'No Results'
  };
  constructor(public adminService: AdminService) { }

  ngOnInit() {
    this.loadData()
  }
  loadData() {
    this.adminService.getClients().subscribe((listItem) => {
      this.listItem = listItem;
    });
  }


}
