import { Component, Input, OnInit, OnChanges, DoCheck, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { ViewChildren } from '@angular/core';
import * as $ from 'jquery';
import { ViewCell } from 'ng2-smart-table';
@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit, OnChanges, DoCheck, AfterViewInit {
    @Input() settings;
    @Input() tableData;
    pages: any[];
    source: any;
    oldFilteredAndSorted: any[];

    constructor() { }
    ngOnInit() {
        this.source.setPaging(1, this.settings.pager.perPage);
    }

    ngOnChanges(changes: any) {
        // console.log(changes.source.currentValue);
        this.source = new LocalDataSource(this.tableData);
        this.source.setPaging(1, this.settings.pager.perPage);
        // $('input-filter input').attr('placeholder', 'Filter');
    }

    ngDoCheck() {
        if (this.source.filteredAndSorted !== this.oldFilteredAndSorted) {
            this.oldFilteredAndSorted = this.source.filteredAndSorted;
        }
    }

    ngAfterViewInit() {
        $('input-filter input').attr('placeholder', 'Filter');
        // Component views are initialized
        // this.chref.detectChanges();
    }



}




