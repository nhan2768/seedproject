import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { DatatableComponent } from './datatable/datatable.component';
import { DialogComponent } from './dialog/dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
    imports: [
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    exports: [
        FormsModule,
        HttpModule,
        CommonModule,
        NgbModule,
        HttpClientModule,
        Ng2SmartTableModule,
        DatatableComponent
    ],
    declarations: [
        DatatableComponent
    ],
})
export class ShareLibModule { }
